let age = prompt("Your age?");
let name = prompt("Your name?");

while (age === "" || isNaN(age)) {
    alert("Please enter a valid age.");
    age = prompt("Your age?");
}

while (name === "") {
    alert("Please enter a valid name.");
    name = prompt("Your name?");
}

console.log(age);
console.log(name);

if (age < 18) {
    alert("You are not allowed to visit this website");
} else if (age >= 18 && age < 22) {
    let isTrue = confirm("Are you sure you want to continue?");
    
    if (isTrue) {
        alert("Welcome, " + name);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert("Welcome, " + name);
}
